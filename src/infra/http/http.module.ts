import { GetRecipientNotifications } from './../../application/entities/use-cases/get-recipient-notifications';
import { CountRecipientNotifications } from './../../application/entities/use-cases/count-recipient-notifications';
import { CancelNotification } from './../../application/entities/use-cases/cancel-notification';
import { Module } from '@nestjs/common';
import { SendNotification } from 'src/application/entities/use-cases/send-notification';
import { DatabaseModule } from '../database/database.module';
import { NotificationsController } from './controllers/notifications.controller';
import { ReadNotification } from '@application/entities/use-cases/read-notification';
import { UnreadNotification } from '@application/entities/use-cases/unread-notification';

@Module({
  imports: [DatabaseModule],
  controllers: [NotificationsController],
  providers: [
    SendNotification,
    CancelNotification,
    CountRecipientNotifications,
    GetRecipientNotifications,
    ReadNotification,
    UnreadNotification,
  ],
})
export class HttpModule {}
