import { IsNotEmpty, IsUUID, Length } from 'class-validator';

export interface CreateNotificationBodyProps {
  recipientId: string;
  content: string;
  category: string;
}

export class CreateNotificationBody {
  
  @IsNotEmpty()
  @IsUUID()
  recipientId: string;

  @IsNotEmpty()
  @Length(5, 240)
  content: string;

  @IsNotEmpty()
  category: string;

}
